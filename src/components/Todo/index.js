import React from 'react'

import Container from './style'

function Todo({ todo, removeTodo }) {
  return (
    <Container>
      <h1>{todo.title}</h1>
      <button onClick={() => removeTodo(todo)}>
        <i className="material-icons">delete</i>
      </button>
    </Container>
  )
}

export default Todo
