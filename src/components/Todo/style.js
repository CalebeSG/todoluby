import styled from 'styled-components'

const Container = styled.article`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #ddd;
  padding: 10px 0;

  &:last-child {
    border: 0;
  }

  h1 {
    font-size: 16px;
    color: #555;
  }

  button {
    width: 30px;
    height: 30px;
    border: 0;
    color: #666;
    background: transparent;
    cursor: pointer;
    transition: all .4s;

    i {
      font-size: 18px;
    }

    &:hover {
      color: #f35123;
    }
  }
`

export default Container
