import React from 'react'

import { withFormik, Field, ErrorMessage } from 'formik'

import FormTodo from './style'

function Form({ addTodo, handleSubmit, values }) {

  function saveTodo() {
    if(values.title.length >= 5) {
      addTodo({
        id: new Date().getTime(),
        title: values.title
      })
    }
  }

  return (
    <FormTodo onSubmit={handleSubmit}>
      <div className="row">
        <Field 
          type="text"
          name="title"
          placeholder="Entre com sua tarefa"
        />
        <button type="submit" onClick={saveTodo}>+</button>
      </div>
      <ErrorMessage name="title" >
        { message => <span>{message}</span>}
      </ErrorMessage>
    </FormTodo>
  )
}

export default withFormik({
  mapPropsToValues(props) {
    return {
      title: '',
    }
  },

  validate(values) {
    const errors = {}
    const { title } = values

    if(title.length < 5 && title.length > 0) {
      errors.title = 'A tarefa deve ter no mínimo 5 caracteres'
    }

    return errors
  },

  handleSubmit(values, formikBag) {
    formikBag.setSubmitting(false)

    formikBag.resetForm()
  }
})(Form)
