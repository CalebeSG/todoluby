import styled from 'styled-components'

const FormTodo = styled.form`
  margin: 50px 0;
  width: 100%;
  display: flex;
  flex-direction: column;

  .row {
    width: 100%;
    height: 40px;
    display: flex;
    align-items: center;
    box-shadow: 0 0 14px rgba(0, 0, 0, 0.04);
  }

  input {
    flex: 1;
    height: 100%;
    padding: 0 15px;
    border: 0;
    font-size: 14px;
  }

  button {
    height: 100%;
    width: 40px;
    border: 0;
    background-color: #ccc;
    color: #fff;
    font-size: 18px;
    margin-left: -2px;
    cursor: pointer;
    transition: background-color 0.4s;

    &:hover {
      background-color: #02a4ef;
    }
  }

  span {
    font-size: 12px;
    margin: 5px 0 0 5px;
    color: crimson;
  }
`

export default FormTodo
