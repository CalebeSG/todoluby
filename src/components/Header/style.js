import styled from 'styled-components'

const Logo = styled.header`
  max-width: 200px;
  width: 100%;

  img {
    width: 100%;
  }

  p {
    color: #555;
    letter-spacing: 17px;
    text-align: center;
    margin-left: 5px;
  }
`

export default Logo
