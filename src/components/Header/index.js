import React from 'react'

import Logo from './style'

function Header() {
  return (
    <Logo>
      <img src="https://www.luby.com.br/img/Logo-01.png" alt=""/>
      <p>TodoLuby</p>
    </Logo>
  )
}

export default Header
