import { createGlobalStyle } from 'styled-components' 

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    background-color: #f5f5f5;
  }

  body, input, button {
    font-family: 'Roboto', sans-serif;
    color: #333;
  }

  input, button {
    border-radius: 0;
  }
`

export default GlobalStyle
