import styled from 'styled-components'

const Container = styled.div`
  max-width: 700px;
  width: 100%;
  height: 100vh;
  margin: 0 auto;
  padding: 50px 20px 0;

  display: flex;
  flex-direction: column;
  align-items: center;

  .empty {
    font-size: 30px;
    color: #777;
  }
`

export const List = styled.ul`
  width: 100%;
  background-color: #fff;
  border: 0;
  padding: 20px 15px;
  box-shadow: 0 0 14px rgba(0, 0, 0, 0.04);
  margin-bottom: 50px;
`

export const Title = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: 50px;

  span {
    flex: 1;
    height: 1px;
    background-color: #ccc;
  }

  p {
    margin: 0 10px;
    color: #555;
  }
`

export default Container
