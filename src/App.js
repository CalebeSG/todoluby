import React, { useState, useEffect } from 'react'

import Header from './components/Header'
import Form from './components/Form'
import Todo from './components/Todo'

import GlobalStyle from './styles/global'
import Container, { List, Title } from './style'

function App() {
  const [todos, setTodos] = useState([])

  useEffect(() => {
    async function loadInitialTodos() {
      const list = await localStorage.getItem('todos')

      if(list) {
        setTodos(JSON.parse(list))
      }
    }
    loadInitialTodos()
  }, [])

  function addTodo(todo) {
    const list = [...todos, todo]
    saveList(list)
  }

  function removeTodo(todo) {
    const list = todos.filter(item => item.id !== todo.id)
    saveList(list)
  }

  async function saveList(list) {
    await localStorage.setItem('todos', JSON.stringify(list))
    setTodos(list)
  }

  return (
    <>
      <GlobalStyle />
      
      <Container>
        <Header />
        <Form addTodo={addTodo} />

        {(todos.length > 0 ? (

          <>
            <Title>
              <span></span>
              <p>Tarefas Ativas</p>
              <span></span>
            </Title>
          
            <List>
              {todos.map(todo => (
                <Todo key={todo.id} todo={todo} removeTodo={removeTodo} />
              ))}
            </List>
          </>

        ) : ( <div className="empty">Lista Vazia :(</div> ))}
      </Container>
    </>
  )
}

export default App
